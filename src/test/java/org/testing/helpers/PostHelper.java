package org.testing.helpers;

import org.openqa.selenium.By;
import org.testing.AppManager;
import org.testing.bases.HelperBase;
import org.testing.enity.Post;

public class PostHelper extends HelperBase {
    public PostHelper(AppManager appManager) {
        super(appManager);
    }

    public void addPost(Post post) throws InterruptedException {
        sleep(5000);
        driver.findElement(By.className("zgT5MfUrDMC54cpiCpZFu")).click();
        sleep(5000);
        driver.findElement(By.className("PqYQ3WC15KaceZuKcFI02")).sendKeys(post.getTitle());
        driver.findElement(By.className("notranslate")).sendKeys(post.getText());
        driver.findElement(By.className("_1MHSX9NVr4C2QxH2dMcg4M")).click();
        driver.findElement(By.className("_2MAa_9ffQVHzsZ-RD1dD5F")).click();
        sleep(5000);
        driver.findElement(By.className("_18Bo5Wuo3tMV-RDB8-kh8Z")).click();
    }

    public boolean isPostLoaded(Post post) throws InterruptedException {
        sleep(5000);
        return driver.findElement(By.className("_eYtD2XCVieq6emjKBH3m")).getText().equals(post.getTitle())
                && driver.findElement(By.className("_1qeIAgB0cPwnLhDF9XSiJM")).getText().equals(post.getText());
    }
}
