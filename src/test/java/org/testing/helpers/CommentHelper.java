package org.testing.helpers;

import org.openqa.selenium.By;
import org.testing.AppManager;
import org.testing.bases.HelperBase;
import org.testing.enity.Comment;

public class CommentHelper extends HelperBase {

    public CommentHelper(AppManager appManager) {
        super(appManager);
    }

    public void addCommentToPost(Comment comment) throws InterruptedException {
        sleep(5000);
        driver.findElement(By.className("_50RxI-5rW1xzwoC42vhzM")).click();
        sleep(5000);
        driver.findElement(By.linkText("My Profile")).click();
        sleep(5000);
        driver.findElement(By.className("FHCV02u6Cp2zYL0fhQPsO")).click();
        sleep(5000);
        driver.findElement(By.className("notranslate")).sendKeys(comment.getText());
        driver.findElement(By.className("_22S4OsoDdOqiM-hPTeOURa")).click();
        sleep(5000);
    }

    public boolean isCommentOnThePage(Comment comment) throws InterruptedException {
        String text = comment.getText();
        sleep(5000);
        return driver.findElement(By.className("_1YCqQVO-9r-Up6QPB9H6_4"))
                .findElement(By.className("_1qeIAgB0cPwnLhDF9XSiJM")).getText().equals(text);
    }
}
