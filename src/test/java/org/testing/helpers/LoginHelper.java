package org.testing.helpers;

import org.openqa.selenium.By;
import org.testing.AppManager;
import org.testing.bases.HelperBase;
import org.testing.enity.SignInParams;

public class LoginHelper extends HelperBase {

    public LoginHelper(AppManager appManager) {
        super(appManager);
    }

    public void login(SignInParams signInParams) throws InterruptedException {
        String login = signInParams.getLogin();
        String password = signInParams.getPassword();
        sleep(5000);
        if (isLoggedIn()) {
            sleep(5000);
            if (isLoggedIn(signInParams)) {
                return;
            }
            logout();
        }
        appManager.getNavigateHelper().getLoginPage();
        driver.findElement(By.id("loginUsername")).sendKeys(login);
        driver.findElement(By.id("loginPassword")).sendKeys(password);
        driver.findElement(By.cssSelector(".m-full-width")).click();
        sleep(5000);
    }

    public void logout() {
        if (isLoggedIn()) {
            driver.findElement(By.className("_3KfbpxpA8Esu_3UHTmIvfw")).click();
            driver.findElement(By.className("vzhy90YD0qH7ZDJi7xMGw")).click();
        }
    }

    public boolean isLoggedIn() {
        return isElementPresent(By.className("XHbKeEqnW58ib9mTN6jnS"));
    }

    public boolean isLoggedIn(SignInParams signInParams) {
        boolean isLoggedin = isLoggedIn();
        return isLoggedin && signInParams.getLogin().equals(getLoggedUserName());
    }

    public String getLoggedUserName() {
        return driver.findElement(By.className("_2BMnTatQ5gjKGK5OWROgaG")).getText();
    }
}
