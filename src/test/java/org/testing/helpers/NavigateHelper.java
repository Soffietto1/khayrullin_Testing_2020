package org.testing.helpers;

import org.testing.AppManager;
import org.testing.bases.HelperBase;

public class NavigateHelper extends HelperBase {

    private String baseUrl;

    public NavigateHelper(AppManager appManager, String baseUrl) {
        super(appManager);
        this.baseUrl = baseUrl;
    }

    public void getLoginPage() {
        appManager.getDriver().get(baseUrl + "/login");
    }

    public void getBasePage() throws InterruptedException {
        appManager.getDriver().get(baseUrl);
        sleep(5000);
    }


}
