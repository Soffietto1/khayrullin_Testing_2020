package org.testing.tests;

import com.tngtech.java.junit.dataprovider.DataProvider;
import com.tngtech.java.junit.dataprovider.DataProviderRunner;
import com.tngtech.java.junit.dataprovider.UseDataProvider;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.testing.bases.AuthBase;
import org.testing.enity.Comment;
import org.testing.util.TestDataGenerator;

import java.util.List;

@RunWith(DataProviderRunner.class)
public class CommentTest extends AuthBase {

    @DataProvider
    public static List<Comment> commentDataProvider() {
        TestDataGenerator.generateCommentData();
        return TestDataGenerator.getCommentsData();
    }

    @Test
    @UseDataProvider("commentDataProvider")
    public void adding_new_comment_test(Comment comment) throws Exception {
        appManager.getCommentHelper().addCommentToPost(comment);
        boolean isCommentLoaded = appManager.getCommentHelper().isCommentOnThePage(comment);
        Assert.assertTrue(isCommentLoaded);
        appManager.getNavigateHelper().getBasePage();
    }
}
