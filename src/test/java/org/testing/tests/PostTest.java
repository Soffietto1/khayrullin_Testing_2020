package org.testing.tests;

import com.tngtech.java.junit.dataprovider.DataProvider;
import com.tngtech.java.junit.dataprovider.DataProviderRunner;
import com.tngtech.java.junit.dataprovider.UseDataProvider;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.testing.bases.AuthBase;
import org.testing.enity.Post;
import org.testing.util.TestDataGenerator;

import java.util.List;

@RunWith(DataProviderRunner.class)
public class PostTest extends AuthBase {

    @DataProvider
    public static List<Post> postDataProvider() {
        TestDataGenerator.generatePostData();
        return TestDataGenerator.getPostData();
    }

    @Test
    @UseDataProvider("postDataProvider")
    public void adding_new_post_test(Post post) throws Exception {
        appManager.getPostHelper().addPost(post);
        boolean isPostLoaded = appManager.getPostHelper().isPostLoaded(post);
        Assert.assertTrue(isPostLoaded);
        appManager.getNavigateHelper().getBasePage();
    }
}
