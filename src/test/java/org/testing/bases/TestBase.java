package org.testing.bases;

import org.junit.After;
import org.junit.Before;
import org.testing.AppManager;

import static org.junit.Assert.fail;

public class TestBase {

    public AppManager appManager;

    @Before
    public void setUp() {
        appManager = AppManager.getInstance();
    }

    @After
    public void tearDown() {
        String verificationErrorString = appManager.getVerificationErrors().toString();
        if (!"".equals(verificationErrorString)) {
            fail(verificationErrorString);
        }
    }

}