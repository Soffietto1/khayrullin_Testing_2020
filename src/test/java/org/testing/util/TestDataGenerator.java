package org.testing.util;

import com.fasterxml.jackson.core.util.DefaultPrettyPrinter;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import lombok.experimental.UtilityClass;
import org.testing.enity.Comment;
import org.testing.enity.Post;

import java.io.File;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

@UtilityClass
public class TestDataGenerator {

    private static final ObjectMapper objectMapper = new ObjectMapper();


    private static final String JSONS_PATH = "src/test/resources/jsons/";
    private static final String POST_JSON_PATH = JSONS_PATH + "post.json";
    private static final String COMMENT_JSON_PATH = JSONS_PATH + "comments.json";
    private static final int POST_COUNT = 3;
    private static final int COMMENT_COUNT = 3;
    private static final int POST_CHAR_COUNT = 10;
    private static final int COMMENT_CHAR_COUNT = 20;

    public void generatePostData() {
        ObjectWriter writer = objectMapper.writer(new DefaultPrettyPrinter());
        try {
            writer.writeValue(new File(POST_JSON_PATH), getRandomPosts(POST_COUNT));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public List<Post> getPostData() {
        ObjectMapper mapper = new ObjectMapper();
        try {
            return Arrays.asList(mapper.readValue(Paths.get(POST_JSON_PATH).toFile(), Post[].class));
        } catch (IOException e) {
            e.printStackTrace();
            return new ArrayList<>();
        }
    }

    public List<Comment> getCommentsData() {
        ObjectMapper mapper = new ObjectMapper();
        try {
            return Arrays.asList(mapper.readValue(Paths.get(COMMENT_JSON_PATH).toFile(), Comment[].class));
        } catch (IOException e) {
            e.printStackTrace();
            return new ArrayList<>();
        }
    }

    public void generateCommentData() {
        ObjectWriter writer = objectMapper.writer(new DefaultPrettyPrinter());
        try {
            writer.writeValue(new File(COMMENT_JSON_PATH), getRandomComments(COMMENT_COUNT));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private Post[] getRandomPosts(int count) {
        List<Post> posts = new ArrayList<>();
        for (int i = 0; i < count; i++) {
            posts.add(new Post(randomString(POST_CHAR_COUNT), randomString(POST_CHAR_COUNT)));
        }
        return posts.toArray(new Post[0]);
    }

    private Comment[] getRandomComments(int count) {
        List<Comment> comments = new ArrayList<>();
        for (int i = 0; i < count; i++) {
            comments.add(new Comment(randomString(COMMENT_CHAR_COUNT)));
        }
        return comments.toArray(new Comment[0]);
    }

    private String randomString(int charCount) {
        Random randomizer = new Random();
        StringBuilder stringBuilder = new StringBuilder();
        for (int i = 0; i < charCount; i++) {
            char randomChar = (char) (randomizer.nextInt(67) + 57);
            stringBuilder.append(randomChar);
        }
        return stringBuilder.toString();
    }
}
